pragma ton-solidity >= 0.35.0;
pragma AbiHeader expire;

contract FeeContract {

    address owner;
    
    constructor(address ownerAdress) public {
        require(msg.pubkey() == tvm.pubkey(), 100);
        tvm.accept();
        owner = ownerAdress;
    }

    function sendValue(address dest, uint128 amount, bool bounce) public{
        require(msg.pubkey() == tvm.pubkey(), 100);
        tvm.accept();
        owner.transfer(amount / 15, bounce, 0);
        dest.transfer(amount - (amount  / 15), bounce, 0);
    }
}
