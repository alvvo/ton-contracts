#!/bin/bash

TON_CLI="/home/nikita/tonos-cli/tonos-cli"
TON_COMPILER="/home/nikita/TON-Solidity-Compiler/build/solc/solc"
TVM_LINKER="/home/nikita/TVM-linker/tvm_linker/target/release/tvm_linker"

$TON_COMPILER FeeContract.sol 
$TVM_LINKER compile FeeContract.code --lib /home/nikita/TON-Solidity-Compiler/lib/stdlib_sol.tvm