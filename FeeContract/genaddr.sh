#!/bin/bash

TON_CLI="/home/nikita/tonos-cli/tonos-cli"
TVC_FILE = $1

$TON_CLI config --url localhost
$TON_CLI genaddr --genkey FeeContract.keys.json --wc 0 $1 FeeContract.abi.json